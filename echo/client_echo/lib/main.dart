import 'dart:io';

import 'package:flutter/material.dart';

//Esegue l'applicazione
void main() {
  runApp(App());
}

//Classe per la creazione dell'app
class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'echoApp',
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          color: Colors.red,
        ),
        brightness: Brightness.light,
        primaryColor: Colors.black,
      ),
      home: HomePage(),
    );
  }
}

//
class HomePage extends StatefulWidget {
  final String title;
  HomePage({Key key, this.title}) : super(key: key);
  @override
  HomePageState createState() => HomePageState();
}

//Main class
class HomePageState extends State<HomePage> {
  //Variabili principali
  String _ip = "192.168.4.87";
  int _port = 3000;
  String _msg = "";
  String _output = "";
  String _pulsText = "Connect";
  //String _connection = "DISCONNECTED";
  Color _color = Colors.red;
  Socket _s;

  //Connessione socket
  void socketC() {
    if (_pulsText == "Connect") {
      Socket.connect(_ip, _port).then(
        (ss) {
          setState(() {
            _s = ss;
            _color = Colors.green;
            _pulsText = "Disconnect";
          });
          _s.listen(socketL);
          print('*** Connected to: ${_s.remoteAddress.address}:${_s.remotePort}');
        },
      );
      
    }
    if (_pulsText == "Disconnect") {
      _s.close();
      setState(() {
        _color = Colors.red;
        _pulsText = "Connect";
      });
    }
  }

  //Ascolto per il messaggio
  void socketL(data) {
    setState(() {
      _output = String.fromCharCodes(data).trim();
    });
  }

  //Invio messaggio
  void socketW() {
    _s.write(_msg);
  }

  Widget connectBox() {
    return Container(
        width: 425,
        decoration: BoxDecoration(
            border: Border.all(color: Colors.black),
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 100,
                  decoration: BoxDecoration(
                      color: _color,
                      border: Border.all(color: Colors.black),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  alignment: Alignment.center,
                  child: Text(
                    'STATUS',
                    style: TextStyle(fontSize: 25),
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  width: 150,
                ),
                FlatButton.icon(
                    color: Colors.yellow,
                    icon: Icon(Icons.airplay), //`Icon` to display
                    label: Text(
                      _pulsText, //`Text` to display
                      style: TextStyle(fontSize: 20),
                    ),
                    onPressed: () {
                      socketC();
                    },
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0))),
              ],
            ),
            Container(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 80,
                  width: 220,
                  child: TextFormField(
                    style: TextStyle(
                        color: Colors.black, fontFamily: 'SFUIDisplay'),
                    decoration: InputDecoration(
                        labelText: 'IP address',
                        prefixIcon: Icon(Icons.wifi),
                        labelStyle: TextStyle(fontSize: 20)),
                    onChanged: (text) {
                      setState(() {
                        if (text != "") _ip = text;
                      });
                    },
                  ),
                ),
                Container(
                  height: 80,
                  width: 150,
                  child: TextFormField(
                    style: TextStyle(
                        color: Colors.black, fontFamily: 'SFUIDisplay'),
                    decoration: InputDecoration(
                        labelText: 'Port',
                        prefixIcon: Icon(Icons.add),
                        labelStyle: TextStyle(fontSize: 20)),
                    onChanged: (text) {
                      setState(() {
                        if (text != "") _port = int.parse(text);
                      });
                    },
                  ),
                ),
              ],
            ),
          ],
        ));
  }

  Widget sendBox() {
    return Container(
      width: 425,
      decoration: BoxDecoration(
          border: Border.all(color: Colors.black),
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            height: 80,
            width: 300,
            child: TextFormField(
              style: TextStyle(color: Colors.black, fontFamily: 'SFUIDisplay'),
              decoration: InputDecoration(
                  labelText: 'Message [max:300]',
                  prefixIcon: Icon(Icons.wifi),
                  labelStyle: TextStyle(fontSize: 20)),
              onChanged: (text) {
                setState(() {
                  if(text.length > 300)alert("Il messaggio è troppo lungo");
                  if (text != "") _msg = text;
                });
              },
            ),
          ),
          FloatingActionButton(
              child: Icon(Icons.send), //`Icon` to display
              backgroundColor: Colors.lightBlue,
              onPressed: () {
                setState(() {
                  if(_pulsText == "Connect") {
                    alert("Non sei connesso al server!");
                  }else socketW();
                });
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0)))
        ],
      ),
    );
  }

  void alert(String message){
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Center(child: Text('*Alert*', style: TextStyle(fontSize: 20, color: Colors.red)), ),
            content: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children : <Widget>[
                Expanded(
                  child: Text(
                    message,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.red,
                    ),
                  ),
                )
              ],
            ),
            actions: <Widget>[
              FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  })
            ],
          );
        },
      );
  }

  Widget responseBox() {
    return Container(
      width: 425,
      height: 150,
      decoration: BoxDecoration(
          border: Border.all(color: Colors.black),
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: Align(
        alignment: Alignment.center,
        child: Column(
          children: <Widget>[
            Text(
              'Server response',
              style: TextStyle(fontSize: 20, color: Colors.red),
            ),
            Text(
              '$_output',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 20, color: Colors.black),
            ),
          ],
        ),
      ),
    );
  }

  //Crea la parte grafica dell'app
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text(
          'Client echo',
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 30),
        ),
      ),
      body: Column(
        children: <Widget>[
          Container(height: 20),
          connectBox(), //creazione primo container "connectBox"
          Container(height: 50),
          sendBox(), //creazione secondo container "sendBox"
          Container(height: 20),
          responseBox(), //creazione terzo container "responseBox"
        ],
      ),
    );
  } //build
} //HomePageState
