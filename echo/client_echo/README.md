# ECHO PROJECT

L'interfaccia dell'app client è composta da **3 sezioni**.

+ La prima, serve per il **collegamento al "Server"**.
Infatti si possono vedere:
-lo stato della connessione(in alto a sinistra)
-le caselle di input per l'ip e la porta del server(in basso)
-il pulsante per la connessione(in alto a destra)

+ La seconda, serve per **l'invio del messaggio al server**.
E' composta da:
-una casella di input per contenere il messaggio(sulla sinistra)
-un pulsante per l'invio del messaggio(sulla destra)
 nel caso in cui il server non fosse connesso, viene segnalato attraverto un alert.
 nel caso si superi il numero di caratteri consentiti verrà segnalato.

+ La terza parte, serve per la **visualizzazione del messaggio di risposta del server**.

---
---

Il programma del server si trova nel file dart "server_echo" contenuto nella cartella del progetto.
