import 'package:flutter/material.dart';
import 'SinglePlayer.dart';
import 'MultiPlayer.dart';
import 'package:shimmer/shimmer.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: 'Game', home: HomePage());
  }
}

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  _HomePageState();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Column(
            children: <Widget>[
              Expanded(child: Container()),
              Shimmer.fromColors(
                baseColor: Colors.red,
                highlightColor: Colors.blue,
                child: Text(
                  'TRIS',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 150.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(height: 50,),
              RaisedButton(
                  splashColor: Colors.red,
                  hoverElevation: 20,
                  elevation: 40,
                  color: Colors.black,
                  child: Text(
                    "Single Player",
                    style: TextStyle(color: Colors.red, fontSize: 50),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => SinglePlayer()),
                    );
                  }),
              Expanded(child: Container()),
              RaisedButton(
                  splashColor: Colors.blue,
                  hoverElevation: 20,
                  elevation: 40,
                  color: Colors.black,
                  child: Text(
                    "Multiplayer",
                    style: TextStyle(color: Colors.blue, fontSize: 50),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MultiPlayer()),
                    );
                  }),
              Expanded(child: Container()),
            ],
          ),
        ),
      ),
    );
  }
}
