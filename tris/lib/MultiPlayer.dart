import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shimmer/shimmer.dart';

void main() => runApp(MultiPlayer());

class MultiPlayer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: HomePage());
  }
}

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MultiPlayerState();
  }
}

class MultiPlayerState extends State<HomePage> {
  List<List> m;

  MultiPlayerState() {
    reload();
  }

  reload() {
    m = List<List>(3);
    for (var i = 0; i < m.length; i++) {
      m[i] = List(3);
      for (var j = 0; j < m[i].length; j++) {
        m[i][j] = ' ';
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
               Shimmer.fromColors(
                baseColor: Colors.red,
                highlightColor: Colors.blue,
                child: Text(
                  'MULTIPLAYER',
                  textAlign: TextAlign.center,
                  style: GoogleFonts.lato(fontStyle: FontStyle.italic, fontSize: 50),
                  
                ),
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  buildEl(0, 0),
                  buildEl(0, 1),
                  buildEl(0, 2),
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  buildEl(1, 0),
                  buildEl(1, 1),
                  buildEl(1, 2),
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  buildEl(2, 0),
                  buildEl(2, 1),
                  buildEl(2, 2),
                ],
              ),
            ],
          ),
        ));
  }

  String lastChar = 'o';

  buildEl(int i, int j) {
    return GestureDetector(
      onTap: () {
        changeMatrixField(i, j);
        vincitore(i, j);
      },
      child: Container(
        width: 120.0,
        decoration: BoxDecoration(
            shape: BoxShape.rectangle, border: Border.all(color: Colors.black)),
        child: Center(
          child: Text(
            m[i][j],
            style: TextStyle(fontSize: 92.0),
          ),
        ),
      ),
    );
  }

  changeMatrixField(int i, int j) {
    setState(() {
      if (m[i][j] == ' ') {
        if (lastChar == 'O')
          m[i][j] = 'X';
        else
          m[i][j] = 'O';
        lastChar = m[i][j];
      }
    });
  }

  vincitore(int x, int y) {
    var colonna = 0, riga = 0, diag = 0, invdiag = 0;
    var n = m.length - 1;
    var player = m[x][y];
    var count = 0;
    for (int i = 0; i < m.length; i++) {
      if (m[x][i] == player) colonna++;
      if (m[i][y] == player) riga++;
      if (m[i][i] == player) diag++;
      if (m[i][n - i] == player) invdiag++;
    }
    if (riga == n + 1 ||
        colonna == n + 1 ||
        diag == n + 1 ||
        invdiag == n + 1) {
      print('$player won');
      alert(player);
      reload();
    }
    for (var x = 0; x < m.length; x++) {
      for (var y = 0; y < m.length; y++) {
        if (m[x][y] != ' ') {
          count++;
          print(count);
        }
      }
    }
    if (count == 9) {
      pareggio();
      reload();
    }
  }

  void alert(var player) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text("WINNER!",textAlign: TextAlign.center, style: GoogleFonts.imprima(fontStyle: FontStyle.italic, fontWeight: FontWeight.bold, color: Colors.red, fontSize: 65),),
          content: Row(
            children: <Widget>[
              Text("$player ", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red, fontSize: 30),),
              Text(" won the game!", style: GoogleFonts.lato(fontStyle: FontStyle.italic, fontWeight: FontWeight.normal, color: Colors.black, fontSize: 30),),
            ],
          ),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void pareggio() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text("PAREGGIO!",textAlign: TextAlign.center, style: GoogleFonts.imprima(fontStyle: FontStyle.italic, fontWeight: FontWeight.bold, color: Colors.red, fontSize: 60),),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
