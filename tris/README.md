# GAME

Per questa consegna ho deciso di sviluppare un applicazione che permettesse di giocare a tris.

## Getting Started

Prima di tutto nonostante sapessi come funzionasse il gioco sono andato a guardarmi su internet le regole.
Fatto ciò avevo le idee chiare su come strutturare il tutto.

### Decisioni progetto

Ho deciso di suddividere l'app in 3 schermate differenti:

- Schermata principale: presenta 2 bottoni che indirizzano alle schermate di gioco
- Schermata Single Player: campo da gioco in giocatore singolo
- Schermata Multiplayer: campo da gioco in multi giocatore

## Deployment

``` dart
Give examples
```

## Built With

- [Dart](https://dart.dev/guides) - Language
- [Flutter](https://flutter.dev/docs) - UI toolkit

## Author

- **Enrico Kogoj** - [GitLab account](https://gitlab.com/enrico0101)

## Acknowledgments

- Hat tip to anyone whose code was used
- Inspiration
- etc
