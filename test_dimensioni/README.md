# TEST_DIMENSIONI

Questa consegna si basa su un Test Psico Attitudinale.

## Getting Started

Prima di tutto nonostante sapessi come funzionasse il gioco sono andato a guardarmi su internet le regole.
Fatto ciò avevo le idee chiare su come strutturare il tutto.

### Decisioni progetto

Ho deciso di suddividere l'app in 3 schermate differenti:

- Schermata principale: _presenta 2 bottoni che indirizzano alle schermate di gioco_.
- Schermata Single Player: _campo da gioco in giocatore singolo_
- Schermata Multiplayer: _campo da gioco in multi giocatore_

___

## Deployment

### Main

In questa classe viene preparata la schermata del gioco, con tre target e tre draggable.

### Result page

Invece in questa classe vengono mostrati i risultati:

- Se l'inserimento deglio oggetti è avvenuto seguendo le dimensioni corrette o no.
- Un'anteprima delle mosse fatte.
- Infine un pulsante per poter riavviare il gioco.

### Draggable

Gli oggetti draggable hanno due "stati":

``` dart
child: isD ? Container(width: l, height: l) : Draggable(
```

Nel primo caso se sono già stati spostati su un target sono dei container.
Nel secondo invece sono dei Draggable.

### Target

Gli oggetti target accettano i Draggable, ma alla fine per controllare il risultato verrà controllato se il numero assegnato ai due corrisponde.

``` dart
widget.test(r, data == widget.number);
```

___

## Built With

- [Dart](https://dart.dev/guides) - Programming language
- [Flutter](https://flutter.dev/docs) - UI toolkit

## Author

- **Enrico Kogoj** - [GitLab account](https://gitlab.com/enrico0101)
