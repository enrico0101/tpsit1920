import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'main.dart';
import 'resultPage.dart';
import 'target.dart';
import 'draggable.dart';

var temp;

class Prepare extends StatefulWidget {
  Prepare({Key key}) : super(key: key);

  @override
  PrepareState createState() => PrepareState();
}

class PrepareState extends State<Prepare> {
  @override
  void initState() {
    super.initState();
    startTime();
  }

  startTime() async {
    var duration = new Duration(seconds: 30);
    return Timer.periodic(duration, (Timer t) {
      setState(() {
        temp = t;
      });
    });
  }

  List<double> lList = [70, 100, 150];
  var draggableList = [];
  Random random1 = Random();
  List<DraggableSquare> dsList = [];
  String result = "";
  List<bool> resultList2 = [];

  void testGame(String string1, bool b1) {
    result = "$result" + "$string1";
    resultList2.add(b1);
    if (resultList2.length == 3) {
      print(result);
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => MyPage(result: result, test: resultList2)),
      );
    }
  }

  void setL() {
    List<int> xList = [];
    for (int i = 0; xList.length < 3;) {
      var x = random1.nextInt(3);
      if (!xList.contains(x)) {
        xList.add(x);
        print(xList[i]);
        draggableList.add(MyDraggable(l: lList[x], number: x));
        i++;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    setL();
    return Scaffold(
      appBar: cavalloMain.myAppBar(),
      body: Center(
        child: Column(
          children: <Widget>[
            Spacer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                    Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <
                    Widget>[
                  MyTarget(w: 70, h: 70, number: 0, test: testGame, accept: true),
          SizedBox(height: 20,),
                  MyTarget(w: 100, h: 100, number: 1, test: testGame, accept: true),
          SizedBox(height: 20,),
                  MyTarget(w: 150, h: 150, number: 2, test: testGame, accept: true),
          SizedBox(height: 20,),
                ]),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    draggableList[0],
                    SizedBox(height: 20,),
                    draggableList[1],
                    SizedBox(height: 20,),
                    draggableList[2],
                    SizedBox(height: 20,),
                  ],
                ),
                
              ],
            ),
            Spacer(),
          back(context),
          SizedBox(height: 20,)
          ],
        ),
      ),
    );
  }

  void tempoScaduto() {
    alert();
  }

  void alert() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            "TEMPO SCADUTO!",
            textAlign: TextAlign.center,
            style: GoogleFonts.atomicAge(
                fontStyle: FontStyle.italic,
                fontWeight: FontWeight.bold,
                color: Colors.red,
                fontSize: 45),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Restart"),
              onPressed: () {
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Prepare(),
                    ));
              },
            ),
          ],
        );
      },
    );
  }

  Widget back(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);

        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => (Main()),
            ));
      },
      child: Container(
        width: 200,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            gradient: LinearGradient(colors: <Color>[
              Colors.green.withOpacity(0.99),
              Colors.greenAccent.withOpacity(0.99)
            ], begin: Alignment.topLeft, end: Alignment.bottomRight),
            boxShadow: [
              BoxShadow(
                  offset: Offset(0, 4),
                  blurRadius: 10,
                  color: Colors.grey[400]),
            ]),
        child: Row(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(40),
                      gradient: LinearGradient(colors: <Color>[
                        Colors.greenAccent.withOpacity(0.1),
                        Colors.greenAccent.withOpacity(0.1)
                      ], begin: Alignment.topLeft, end: Alignment.bottomRight),
                      boxShadow: [
                        BoxShadow(
                            offset: Offset(0, 2),
                            blurRadius: 2,
                            color: Colors.green),
                      ]),
                ),
                Icon(
                  Icons.arrow_left,
                  color: Colors.white,
                  size: 50,
                )
              ],
            ),
            Expanded(
              child: Container(),
            ),
            Text(
              "MENU",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 30,
                color: Colors.white,
              ),
            ),
            Expanded(
              child: Container(),
            ),
          ],
        ),
      ),
    );
  }
}
