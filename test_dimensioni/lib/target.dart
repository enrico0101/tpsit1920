import 'package:flutter/material.dart';

class MyTarget extends StatefulWidget {
  MyTarget({Key key, this.h, this.w, this.number, this.test, this.accept})
      : super(key: key);

  final double h, w;
  final int number;
  final Function test;
  bool accept;

  @override
  _MyTargetState createState() => _MyTargetState();
}

class _MyTargetState extends State<MyTarget> {
  Widget c;

  @override
  Widget build(BuildContext context) {
    return DragTarget(
      builder:
          (BuildContext context, List<int> candidateData, List rejectedData) {
        return Container(
          width: widget.w,
          height: widget.h,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.green, width: 3),
          ),
          child: c,
        );
      },
      onWillAccept: (data) {
        if (widget.accept) {
          return true;
        } else
          return false;
      },
      onAccept: (data) {
        widget.accept = false;
        String r = "$data" + "->" + "${widget.number}" + "  ";
        widget.test(r, data == widget.number);
        c = Container(
          child: Center(
            child: Text("", style: TextStyle(fontSize: 25)),
          ),
          color: Colors.green,
        );
      },
    );
  }
}
