import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:test_dimensioni/prepare.dart';
import 'resultPage.dart';
import 'target.dart';
import 'draggable.dart';

MainState cavalloMain;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Main(),
    );
  }
}

class Main extends StatefulWidget {
  Main({Key key}) : super(key: key);

  @override
  MainState createState() => cavalloMain = MainState();
}

class MainState extends State<Main> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: myAppBar(),
      body: Center(
        child: Column(
          children: <Widget>[
            Spacer(),
            Image.asset("images/main.png"),
            Spacer(),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Prepare(),
                    ));
              },
              child: Container(
                width: 200,
                height: 100,
                decoration: BoxDecoration(
                    color: Colors.green, borderRadius: BorderRadius.circular(30)),
                child: Center(
                    child: Text("PLAY",
                        style: GoogleFonts.electrolize(
                            fontStyle: FontStyle.italic,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                            fontSize: 35))),
              ),
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }

  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  Widget myAppBar() {
    return GradientAppBar(
      backgroundColorEnd: Colors.greenAccent,
      backgroundColorStart: Colors.green,
      title: Row(
        children: <Widget>[
          Text("TEST DIMENSIONI",
              style: TextStyle(
                fontSize: 30,
              )),
        ],
      ),
    );
  }
}
