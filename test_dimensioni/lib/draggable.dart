import 'package:flutter/material.dart';

class MyDraggable extends StatefulWidget {
  MyDraggable({Key key, this.number, this.l}) : super(key: key);

  final int number;
  final double l;

  @override
  DraggableSquare createState() => DraggableSquare();
}

class DraggableSquare extends State<MyDraggable> {
  bool isD = false;

  @override
  Widget build(BuildContext context) {
    int number = widget.number;
    double l = widget.l;
    return Container(
      child: isD
          ? Container(width: l, height: l)
          : Draggable(
              data: number,
              child: Container(
                width: l,
                height: l,
                color: Colors.greenAccent,
              ),
              childWhenDragging: Container(
                width: l,
                height: l,
              ),
              feedback: Container(
                width: l,
                height: l,
                color: Colors.green.withOpacity(0.1),
              ),
              onDragCompleted: () {
                print("completato");
                setState(() {
                  isD = true;
                });
              },
            ),
    );
  }
}
