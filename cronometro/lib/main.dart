import 'package:flutter/material.dart';

//Esegue l'applicazione
void main() {
  runApp(App());
}

//Classe per la creazione dell'app
class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CronometroApp',
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.black,
      ),
      home: HomePage(),
    );
  }
}

//
class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  HomePageState createState() => HomePageState();
}

//Main class
class HomePageState extends State<HomePage> {
  //Variabili principali
  int i = 0;
  int _h = 0;
  int _min = 0;
  int _sec = 0;
  var _state = "pause";
  var _rec = "";

  //Metodo per la gestione della stream
  stream() {
    Stream<int> asc = _timedCounter();
    asc.listen((data) {
      setState(() {
        _sec = data;
        if (i == 60) {
          print("i=60");
          _min++;
          i = 0;
        }
        if (_min == 60) {
          print("min=60");
          i = 0;
          _h++;
          _min = 0;
        }
      });
    });
  }

  //Metodo per la gestione della stream(2)
  Stream<int> _timedCounter() async* {
    //i = 0;
    while (_state == 'play') {
      await Future.delayed(Duration(seconds: 1));
      yield i++;
    }
  }

  //Crea il pulsante "play"
  Widget _play() {
    return Align(
      child: FlatButton.icon(
          icon: Icon(Icons.play_circle_filled), //`Icon` to display
          label: Text('PLAY'), //`Text` to display
          onPressed: () {
            if (_state == "pause" || _state == "record") {
             
              setState(() {
                _state = "play";
                print("play");
              });
               stream();
            }
          }),
    );
  } //_play

  //Crea il pulsante "pause"
  Widget _pause() {
    return Align(
      child: FlatButton.icon(
          icon: Icon(Icons.pause_circle_filled), //`Icon` to display
          label: Text('PAUSE'), //`Text` to display
          onPressed: () {
            setState(() {
              print("pause");
              _state = "pause";
            });
          }),
    );
  } //_pause

  //Crea il pulsante "restart"
  Widget _restart() {
    return Align(
      child: FlatButton.icon(
        icon: Icon(Icons.replay), //`Icon` to display
        label: Text('RESTART'), //`Text` to display
        onPressed: () {
          setState(() {
            print("restart");
            if (_state == "pause" || _state == "record" || _state == "play") {
              _h = 0;
              _min = 0;
              _sec = 0;
              i = 0;
              _rec = "";
            }
          });
        },
      ),
    );
  } //_restart

  //Crea il pulsante "record"
  Widget _record() {
    return Align(
      child: FlatButton.icon(
        icon: Icon(Icons.access_time), //`Icon` to display
        label: Text('RECORD'), //`Text` to display
        onPressed: () {
          setState(() {
            _state = "record";
            _rec = '$_h h.$_min m.$_sec s';
            print(_rec);
          });
        },
      ),
    );
  } //_record

  //Crea la parte grafica dell'app
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Cronometro',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 30),
          ),
        ),
        body: Column(mainAxisAlignment: MainAxisAlignment.center, children: <
            Widget>[
          Align(
              alignment: Alignment.topCenter,
              child: new Container(
                  height: 150,
                  width: 500,
                  margin: const EdgeInsets.all(70.0),
                  padding: const EdgeInsets.all(50.0),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.all(Radius.circular(100))),
                  child: Text(
                    '$_h h.$_min m.$_sec s',
                    style: TextStyle(fontSize: 20),
                    textAlign: TextAlign.center,
                  ))),
          Align(
            alignment: Alignment.center,
            child: Text(
              "RECORD:",
              style: TextStyle(fontSize: 20),
            ),
          ),
          Container(
            child: Text(_rec, style: TextStyle(fontSize: 20)),
          ),
          Expanded(
            child:Container(
              height: 60,
            )        
            ),
            
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Container(
              decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.all(Radius.circular(100))),
              height: 60,
              width: 150,
              child: _restart(),
            ),
            Container(width: 20),
            Container(
              decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.all(Radius.circular(100))),
              height: 60,
              width: 150,
              child: _record(),
            ),
          ]),
          Container(height: 20,),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Container(
              decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.all(Radius.circular(100))),
              height: 60,
              width: 150,
              child: _play(),
            ),
            Container(width: 20),
            Container(
              decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.all(Radius.circular(100))),
              height: 60,
              width: 150,
              child: _pause(),
            ),
          ]
          ),
          Container(height: 20),
        ]
        
        )
        );
  } //build
} //HomePageState
