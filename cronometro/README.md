# Cronometro

Applicazione flutter per un cronometro attraverso uno [Stream](https://gitlab.com/divino.marchese/zuccante_src/-/wiki_pages/dart/stream) e anche tramite l'utilizzo dei [Future](https://gitlab.com/divino.marchese/zuccante_src/-/wiki_pages/dart/future).
Sono state riscontrate varie difficoltà, per lo più risolte grazie alla documentazione trovata online.



## Documentazione

- [Documentazione Flutter](https://flutter.dev/docs)
- [Lista Widgets](https://api.flutter.dev/flutter/widgets/widgets-library.html)


