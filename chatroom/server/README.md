# chatroom_server

es006_chatroom_server.dart è stato la base di partenza. Ho apportato modifiche relative al nome utente, utenti connessi.
Inizialmente l'idea era quella di gestire tutti i messaggi da client, però ho dovuto implementare un modo di gestire i nomi utenti. Questo mi ha portato a creare un "codice", per differenziare i messaggi fra client ed invece quelli per la gestione.  

## SPEZZONI DI CODICE SALIENTI

- Nel metodo "messageHandler" ricevuto un messaggio vado a controllare se si tratta di un messaggio "di sistema", per l'aggiunta del nome al cliente attale, o un messaggio "normale", da un client per gli altri.

```dart
    if(message.contains('@'))//Controllo iniziale messaggi di sistema
    if(split[0] == '@name')//Controllo secondario

    '@name#Miki'//esempio di messaggio di sistema,  
    //@name->Identificativo, Miki->Nome utente
  ```

- Nel caso la connessione col server venga interrotta da un client, verrà inviato a tutti gli altri utenti online al momento, che quest'ultimo si è disconnesso.

```dart
  void finishedHandler() {
    //...
    distributeMessage(this, '$username disconnected.');
    //...
  }
  ```
