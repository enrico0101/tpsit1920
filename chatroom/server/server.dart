import 'dart:core';
import 'dart:io';

ServerSocket server;
List<ChatClient> clients = [];

void main() {
  ServerSocket.bind(InternetAddress.anyIPv4, 3000).then((server) {
    server.listen((client) {
      handleConnection(client);
    });
  });
}

void handleConnection(Socket client) {
  print('handleConnection ----------------');

  print('Connection from ${client.remoteAddress.address}:${client.remotePort}');
  var u = ChatClient(client);
  clients.add(u);
  print('numero utenti connessi: ' + clients.length.toString());
   if(clients.length > 1){
     print('Ci sono più utenti connessi');
     u.write(clients.toString());
   }

  print('\handleConnection ----------------');
}

void distributeMessage(ChatClient client, String message) {
  for (ChatClient c in clients) {
    if (c != client) {
      c.write(message);
    }
  }
}

void removeClient(ChatClient client) {
  clients.remove(client);
}

class ChatClient {
  Socket socket;
  String get ip => socket.remoteAddress.address;
  int get port => socket.remotePort;
  var username;

  ChatClient(Socket s) {
    this.socket = s;
    socket.listen(messageHandler,
        onError: errorHandler, onDone: finishedHandler);
  }

  void messageHandler(data){
    print('MessageHandler ----------------');

    String message = new String.fromCharCodes(data).trim();
    print('Message: ' + message);
    var split;
    if(message.contains('@')){
      print('contiene @');
      split = message.split('#'); 
      print('Split: ' + split.toString());
      if(split[0] == '@name'){
        username = split[1];
        distributeMessage(this, username);
      }
    }
    else if(!message.contains('@')){
      print('distribuite');
      distributeMessage(this, message);
    }

    print('\MessageHandler ----------------\n');
  }

  void errorHandler(error) {
    print('${ip}:${port} Error: $error');
    removeClient(this);
    socket.close();
  }

  void finishedHandler() {
    print('${ip}:${port} Disconnected');
    distributeMessage(this, '$username disconnected.');
    removeClient(this);
    socket.close();
  }

  void write(String message) {
    print('Metodo write ChatClient: ' + message.toString());
    socket.write('$message');
  }

  String toString() {
    return ('$username');
  }
}