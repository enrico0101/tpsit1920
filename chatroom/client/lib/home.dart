import 'dart:io';

import 'main.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  Home();

  @override
  HomeState createState() => cavalloHome = HomeState();
}

class HomeState extends State<Home> {
  //Connessione socket
  void socketC() {
    if (color == Colors.red && name != "") {
      Socket.connect(ip, port).then(
        (ss) {
          print('CONNECTED');
          setState(() {
            s = ss;
            color = Colors.green;
            pulsText = "Disconnect";
          });
          s.write('@name#$name');
          s.listen(messageHandler);
          print('*** Connected to: ${s.remoteAddress.address}:${s.remotePort}');
          cavalloMessages.reload();
        },
      );
    }
    if (color == Colors.green) {
      print('DISCONNECTED');
      s.close();
      setState(() {
        color = Colors.red;
        pulsText = "Connect";
        items.clear();
      });
    }
  }

  Widget msgReciveBuilder(var nome, var messaggio, var ora) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(
              width: 20,
            ),
            Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: Column(
                children: <Widget>[
                  Align(child: Text(nome, style: TextStyle(fontSize: 15))),
                  Text(messaggio, style: TextStyle(fontSize: 20)),
                  Align(child: Text(ora)),
                ],
              ),
            ),
            Expanded(child: Container()),
          ],
        ),
        Container(
          height: 5,
        ),
      ],
    );
  }

  Widget msgSysBuilder(String messaggio) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Flexible(
              flex: 1,
              child: Container(),
            ),
            Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: Column(
                children: <Widget>[
                  Text(messaggio, style: TextStyle(fontSize: 15)),
                ],
              ),
            ),
            Flexible(
              flex: 1,
              child: Container(),
            ),
          ],
        ),
        Container(
          height: 5,
        ),
      ],
    );
  }

  void msgSys(String sysMess) {
    if (sysMess.contains('[')) {
      String output = '';
      var utenti = sysMess.substring(1, sysMess.length - 1);
      var utentiSplit = utenti.split(",");
      print('Utenti connessi: ' + utenti + '---------------------------------');
      for (var item in utentiSplit) {
        if (item != ' null') {
          print(item + ' = null ?');
          output = output.toString() + item.toString();
        }
      }
      if (utentiSplit.length - 1 > 1) {
        messaggi.add(msgSysBuilder(output + ' sono connessi.'));
      } else if (utentiSplit.length - 1 <= 1) {
        messaggi.add(msgSysBuilder(output + ' è connesso.'));
      }
    } else if (!sysMess.contains('[')) {
      if (sysMess.contains('disconnected') || sysMess.contains('connected')) {
        messaggi.add(msgSysBuilder(sysMess));
      } else {
        messaggi.add(msgSysBuilder(sysMess + ' connected.'));
      }
    }
    cavalloMessages.reload();
  }

  //Ascolto per il messaggio
  void messageHandler(data) {
    serverAns = String.fromCharCodes(data).trim();
    print("ricevuto: $serverAns");
    var z;
    if (serverAns.contains('#')) {
      z = serverAns.split('#');
      messaggi.add(msgReciveBuilder(
          z[0],
          z[1],
          TimeOfDay.now().hour.toString() +
              "." +
              TimeOfDay.now().minute.toString()));
    } else if (!serverAns.contains('#')) {
      msgSys(serverAns);
    }
    cavalloMessages.reload();
  }

  void alert(String message) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Center(
            child: Text('*Alert*',
                style: TextStyle(fontSize: 20, color: Colors.red)),
          ),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Text(
                  message,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.red,
                  ),
                ),
              )
            ],
          ),
          actions: <Widget>[
            FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                })
          ],
        );
      },
    );
  }

  Widget connectBox() {
    return Container(
        width: 425,
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  FloatingActionButton(
                      backgroundColor: Colors.yellow,
                      focusColor: Colors.white,
                      foregroundColor: Colors.black,
                      hoverColor: Colors.white,
                      splashColor: Colors.white,
                      child: Icon(Icons.airplay), //`Icon` to display
                      onPressed: () {
                        if (name.length < 1) {
                          setState(() {
                            alert('Nome non valido!');
                          });
                        }
                        socketC();
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0))),
                  Container(
                    width: 20,
                  ),
                ],
              ),
            ),
            Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Flexible(child: Container()),
                    Flexible(
                      flex: 100,
                      child: Form(
                        child: TextFormField(
                          style: TextStyle(
                              color: Colors.black, fontFamily: 'SFUIDisplay'),
                          decoration: InputDecoration(
                              labelText: 'IP address*',
                              prefixIcon: Icon(Icons.wifi),
                              labelStyle: TextStyle(fontSize: 20)),
                          onChanged: (text) {
                            setState(() {
                              if (text != "") ip = text;
                            });
                          },
                        ),
                      ),
                    ),
                    Flexible(
                      flex: 100,
                      child: Form(
                        child: TextFormField(
                          style: TextStyle(
                              color: Colors.black, fontFamily: 'SFUIDisplay'),
                          decoration: InputDecoration(
                              labelText: 'Port*',
                              prefixIcon: Icon(Icons.add),
                              labelStyle: TextStyle(fontSize: 20)),
                          onChanged: (text) {
                            setState(() {
                              if (text != "") port = int.parse(text);
                            });
                          },
                        ),
                      ),
                    ),
                    Flexible(child: Container()),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Flexible(
                      child: Form(
                        child: TextFormField(
                          style: TextStyle(
                              color: Colors.black, fontFamily: 'SFUIDisplay'),
                          decoration: InputDecoration(
                              labelText: 'Username*',
                              prefixIcon: Icon(Icons.people),
                              labelStyle: TextStyle(fontSize: 20)),
                          onChanged: (text) {
                            setState(() {
                              if (text != "") name = text;
                            });
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ));
  }

  //Crea la parte grafica dell'app
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        body: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Form(
                child: ListView(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      width: 35,
                      height: 35,
                      decoration: BoxDecoration(
                          color: color,
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.all(Radius.circular(100))),
                    ),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Image.network(
                      'https://pngimage.net/wp-content/uploads/2018/06/user-login-png-transparent-2.png',
                      scale: 1.5,
                    ),
                    Text(
                      "USER LOGIN",
                      style: TextStyle(fontSize: 25),
                    ),
                  ],
                ),
                connectBox(),
              ],
            ))));
  }

  onError() {
    alert('Connessione non riuscita! Per favore controlla i campi inseriti.');
  }
}
