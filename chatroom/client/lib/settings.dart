import 'dart:ui';

import 'package:flutter/material.dart';

import 'main.dart';

class Settings extends StatefulWidget {
  Settings();

  @override
  SettingsState createState() => cavalloSettings = SettingsState();
}

class SettingsState extends State<Settings> {
  Widget connectBox() {
    return Container(
        child: Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                    flex: 4,
                    child: Form(
                      child: Text(
                        'Your ip is: $ip',
                        style: TextStyle(
                            fontSize: 25,
                            color: Colors.black,
                            fontFamily: 'SFUIDisplay'),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                    flex: 3,
                    child: Form(
                      child: Text(
                        'Your port is: $port',
                        style: TextStyle(
                            fontSize: 25,
                            color: Colors.black,
                            fontFamily: 'SFUIDisplay'),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                    flex: 3,
                    child: Form(
                      child: Text(
                        'Your name is: $name',
                        style: TextStyle(
                            fontSize: 25,
                            color: Colors.black,
                            fontFamily: 'SFUIDisplay'),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        )
      ],
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        body: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Form(
                child: ListView(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Text(
                      "SETTINGS & INFO",
                      style: TextStyle(
                            fontSize: 25,
                            color: Colors.red,
                            fontFeatures: [FontFeature.randomize()],
                            fontFamily: 'SFUIDisplay'),
                    ),
                  ],
                ),
                connectBox(),
              ],
            ))));
  }
}
