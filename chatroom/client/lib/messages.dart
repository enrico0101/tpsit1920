// MESSAGES

import 'main.dart';
import 'package:flutter/material.dart';

class Messages extends StatefulWidget {
  Messages();

  @override
  MessagesState createState() => cavalloMessages = MessagesState();
}

class MessagesState extends State<Messages> {
  
  void alert(String message) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Center(
            child: Text('*Alert*',
                style: TextStyle(fontSize: 20, color: Colors.red)),
          ),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Text(
                  message,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.red,
                  ),
                ),
              )
            ],
          ),
          actions: <Widget>[
            FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                })
          ],
        );
      },
    );
  }

  void reload() {
    setState(() {});
  }

  Widget msgSendBuilder(var nome, var messaggio, var ora) {
    return Column(
      children: <Widget>[
        Row(children: <Widget>[
          Expanded(
            child: Container(),
          ),
          Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Column(
              children: <Widget>[
                Text(messaggio, style: TextStyle(fontSize: 20)),
                Text(ora, style: TextStyle(fontSize: 15)),
              ],
            ),
          ),
          Container(
            width: 20,
          ),
        ]),
        Container(
          height: 5,
        )
      ],
    );
  }

  Widget sendBox() {
    return Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
      Flexible(
        flex: 1,
        child: Form(child: Container()),
      ),
      Flexible(
        flex: 8,
        child: Form(
          child: TextFormField(
            style: TextStyle(color: Colors.black, fontFamily: 'SFUIDisplay'),
            decoration: InputDecoration(
                filled: true,
                hintText: 'Scrivi un messaggio...',
                labelStyle: TextStyle(fontSize: 18)),
            onChanged: (text) {
              setState(() {
                if (text.length > 300) alert("Il messaggio è troppo lungo");
                if (text != "") msgS = text;
              });
            },
          ),
        ),
      ),
      Flexible(
        flex: 1,
        child: Form(child: Container()),
      ),
      Flexible(
        flex: 2,
        child: Column(
          children: <Widget>[
            FloatingActionButton(
                child: Icon(Icons.send), //`Icon` to display
                backgroundColor: Colors.lightBlue,
                onPressed: () {
                  if (msgS != "" && name != "") {
                    setState(() {
                      messaggi.add(msgSendBuilder(
                          name,
                          msgS,
                          TimeOfDay.now().hour.toString() +
                              "." +
                              TimeOfDay.now().minute.toString()));
                    });
                    s.write(name + '#' + msgS);
                  }
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0))),
          ],
        ),
      )
    ]);
  }

  Widget title() {
    return Text(
      "CHAT",
      style: TextStyle(fontSize: 30),
    );
  }

  Widget clearMessages() {
    return FloatingActionButton(
        child: Icon(Icons.clear), //`Icon` to display
        backgroundColor: Colors.lightBlue,
        onPressed: () {
          setState(() {
            messaggi.clear();
          });
        },
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)));
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  title(),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    child: clearMessages(),
                  ),
                ],
              ),
            ],
          ),
        ),
        Expanded(
            child: new ListView.builder(
          controller: c,
          addAutomaticKeepAlives: true,
          itemCount: messaggi.length,
          itemBuilder: (BuildContext ctxt, int index) {
            return (messaggi.elementAt(index));
          },
        )),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: sendBox(),
        ),
      ],
    );
  }
}