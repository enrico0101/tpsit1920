# chatroom_client

Client, permette di collegari al server e messaggiare con gli altri utenti online.
Per convenienza e design, l'app è suddivisa in tre parti, grazie ad un BottomNavigationBar, queste sono:

- Home.
- Messagges.
- Settings.

## Home

  Questa schermata è dedicata all'accesso dell'utente. La struttura è molto semplice, presenta 3 TextFormField per l'inserimento di ip, port, username.
  Una volta inseriti si potrà procedere alla connessione al server cliccando il tasto giallo ed accertarsi dell'avvenuta connessione dal pallino di stato posizionato in alto a destra nella pagina (Cambierà colore, da rosso a verde).

### SPEZZONI DI CODICE SALIENTI

Vengono inseriti dati nella lista "messaggi".

```dart
    messaggi.add(...)
  ```

E vengono aggiornati sulla schermata dei messaggi grazie alla variabile globale "cavalloMessages" presente nella classe main.

```dart
    cavalloMessages.reload()//refresh sui messaggi (vedi documentazione sottostante).
  ```

## Messagges

  Questa schermata è dedicata alla chat. Anche questa schermata presenta una struttura molto semplice.
  Una volta inseriti si potrà procedere alla connessione al server cliccando il tasto giallo ed accertarsi dell'avvenuta connessione dal pallino di stato posizionato in alto a destra nella pagina (Cambierà colore, da rosso a verde).

### SPEZZONI DI CODICE SALIENTI

Vengono inseriti dati nella lista "messaggi".

```dart
    messaggi.add(...)
  ```

Sono presenti l'assegnazione della variabile "cavalloMessages" e il metodo reload.

```dart
    MessagesState createState() => cavalloMessages = MessagesState();//init

    void reload() {//metodo messo a disposizione per aggiornare la schermata
    setState(() {});
    }
  ```

## Settings

  Questa schermata, dedicata alle informazioni attuali delle variabili più importati; in realtà era nata con l'idea di avere delle impostazioni quali: selezione colori di sfondo e testo, dimensione carattere ed altre "chicche" però a causa di vari problemi e tempistiche, non è stata sviluppata come da idea.
