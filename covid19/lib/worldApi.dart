import 'dart:convert';
import 'package:http/http.dart' as http;

Future<List<WorldData>> getWorldData() async {
  final response = await http.get('https://corona.lmao.ninja/v2/countries/');
  if (response.statusCode == 200) {
    final parsed = json.decode(response.body);
    //print('body parsed $parsed');
    return parsed.map<WorldData>((json) => WorldData.fromJson(json)).toList();
  } else {
    throw Exception('Failed to get Data');
  }
}

class WorldData {
  /*
  {
    "updated":1588266649331,
    "country":"Afghanistan",
    "countryInfo":
    {
      "_id":4,
      "iso2":"AF",
      "iso3":"AFG",
      "lat":33,
      "long":65,
      "flag":"https://corona.lmao.ninja/assets/img/flags/af.png"
    },
    "cases":2171,
    "todayCases":232,
    "deaths":64,
    "todayDeaths":4,
    "recovered":260,
    "active":1847,
    "critical":7,
    "casesPerOneMillion":56,
    "deathsPerOneMillion":2,
    "tests":10022,
    "testsPerOneMillion":257,
    "continent":"Asia"
  }
  */

  int updated;
  String country;
  CountryInfo countryInfo;
  int cases;
  int todayCases;
  int deaths;
  int todayDeaths;
  int recovered;
  int active;
  int critical;
  int casesPerOneMillion;
  int deathsPerOneMillion;
  int tests;
  int testsPerOneMillion;
  String continent;

  WorldData(
      {this.updated,
      this.country,
      this.countryInfo,
      this.cases,
      this.todayCases,
      this.deaths,
      this.todayDeaths,
      this.recovered,
      this.active,
      this.critical,
      this.casesPerOneMillion,
      this.deathsPerOneMillion,
      this.tests,
      this.testsPerOneMillion,
      this.continent});

  WorldData.fromJson(Map<String, dynamic> json) {
    updated = json['updated'];
    country = json['country'];
    countryInfo = json['countryInfo'] != null
        ? new CountryInfo.fromJson(json['countryInfo'])
        : null;
    cases = json['cases'];
    todayCases = json['todayCases'];
    deaths = json['deaths'];
    todayDeaths = json['todayDeaths'];
    recovered = json['recovered'];
    active = json['active'];
    critical = json['critical'];
    casesPerOneMillion = json['casesPerOneMillion'];
    deathsPerOneMillion = json['deathsPerOneMillion'];
    tests = json['tests'];
    testsPerOneMillion = json['testsPerOneMillion'];
    continent = json['continent'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['updated'] = this.updated;
    data['country'] = this.country;
    if (this.countryInfo != null) {
      data['countryInfo'] = this.countryInfo.toJson();
    }
    data['cases'] = this.cases;
    data['todayCases'] = this.todayCases;
    data['deaths'] = this.deaths;
    data['todayDeaths'] = this.todayDeaths;
    data['recovered'] = this.recovered;
    data['active'] = this.active;
    data['critical'] = this.critical;
    data['casesPerOneMillion'] = this.casesPerOneMillion;
    data['deathsPerOneMillion'] = this.deathsPerOneMillion;
    data['tests'] = this.tests;
    data['testsPerOneMillion'] = this.testsPerOneMillion;
    data['continent'] = this.continent;
    return data;
  }
}

class CountryInfo {
  int iId;
  String iso2;
  String iso3;
  var lat;
  var long;
  String flag;

  CountryInfo({this.iId, this.iso2, this.iso3, this.lat, this.long, this.flag});

  CountryInfo.fromJson(Map<String, dynamic> json) {
    iId = json['_id'];
    iso2 = json['iso2'];
    iso3 = json['iso3'];
    lat = json['lat'];
    long = json['long'];
    flag = json['flag'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.iId;
    data['iso2'] = this.iso2;
    data['iso3'] = this.iso3;
    data['lat'] = this.lat;
    data['long'] = this.long;
    data['flag'] = this.flag;
    return data;
  }
}
